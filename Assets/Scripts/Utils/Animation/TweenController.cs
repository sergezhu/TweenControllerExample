﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TweenController : MonoBehaviour 
{
    private static TweenController _instance;

    private const int EASING_CURVE_STEPS = 100;

    public TimelineController timelineController { get; private set; }

    public enum EasingType
    {
        CubicInEasing,
        CubicOutEasing,
        CubicOutInverseEasing,
        QuadInEasing,
        QuadOutEasing,
        LinearEasing
    }

    public class TimelineController
    {
        private float _start;
        private float _offset;

        private TimelinePoint _tempPoint;
        private List<TimelinePoint> _timeline;

        private bool _isReady;

        public TimelineController()
        {
            _start = 0;
            _offset = 0;
            _isReady = true;

            _timeline = new List<TimelinePoint>();
        }

        public List<TimelinePoint> GetPoints()
        {
            return _timeline;
        }

        public bool isReady()
        {
            return _isReady;
        }

        private void AddSeparateAnimation(SeparateAnimation sa)
        {
            if ((sa != null) && isReady())
            {
                _tempPoint = new TimelinePoint(sa);
                _timeline.Add(_tempPoint);
            }
            else
            {
                //Debug.Log("Error: Animation is adding when other animation is running");
            }
        }

        public void AddPositionAnimation(GameObject target, Vector3 endP, float duration = 1.0f, float delay = 0, EasingType eType = EasingType.LinearEasing)
        {
            AddSeparateAnimation(new PositionAnimation(target, endP, duration, delay, eType));
        }

        public void AddLocalPositionAnimation(GameObject target, Vector3 endLP, float duration = 1.0f, float delay = 0, EasingType eType = EasingType.LinearEasing)
        {
            AddSeparateAnimation(new LocalPositionAnimation(target, endLP, duration, delay, eType));
        }

        public void AddShakeLocalPositionAnimation(GameObject target, Vector3 strength, float duration = 1.0f, float delay = 0, EasingType eType = EasingType.CubicOutInverseEasing)
        {
            AddSeparateAnimation(new ShakeLocalPositionAnimation(target, strength, duration, delay, eType));
        }

        public void AddRotateAnimation(GameObject target, Quaternion deltaQ, float duration = 1.0f, float delay = 0, EasingType eType = EasingType.LinearEasing)
        {
            AddSeparateAnimation(new RotateAnimation(target, deltaQ, duration, delay, eType));
        }

        public void AddLocalRotateAnimation(GameObject target, Quaternion deltaQ, float duration = 1.0f, float delay = 0, EasingType eType = EasingType.LinearEasing)
        {
            AddSeparateAnimation(new LocalRotateAnimation(target, deltaQ, duration, delay, eType));
        }

        public void AddShakeLocalRotateAnimation(GameObject target, Vector3 strength, float duration = 1.0f, float delay = 0, EasingType eType = EasingType.CubicOutInverseEasing)
        {
            AddSeparateAnimation(new ShakeLocalRotateAnimation(target, strength, duration, delay, eType));
        }
        

        public void StartAnimation()
        {           
            if((_timeline.Count > 0) && _isReady)
            {
                _isReady = false;
                _start = Time.time;
                //_offset += Time.deltaTime;

                foreach (TimelinePoint tp in _timeline)
                {
                    tp.UpdateBounds();
                    tp.GetSA().SetStarted(false);
                    tp.GetSA().SetFinished(false);
                }
            }
            else
            {
                //Debug.Log("Previous running Animation is not finished! Running " + _timeline.Count + " animations");
            }
        }

        public void CheckAnimation()
        {
            //Debug.Log("Length (tp): " + _timeline.Count);
            if(_timeline.Count > 0)
            {
                foreach (TimelinePoint tp in _timeline.ToArray())
                {                             
                    if(Time.time >= tp.GetInTime())
                    {                                     
                        if(!tp.GetSA().GetStarted()) tp.GetSA().SetStarted(true);

                        if (tp.GetSA().GetStarted() && (!tp.GetSA().GetFinished())) {

                            float _complete = Mathf.Clamp((Time.time - tp.GetInTime()) / (tp.GetOutTime() - tp.GetInTime()), 0f, 1f);
                            tp.GetSA().UpdateAn(_complete);                           
                        }

                        if (Time.time >= tp.GetOutTime())
                        {
                            tp.GetSA().SetFinished(true);
                            _timeline.Remove(tp);
                        }
                    }
                }
            }
            else
            {
                _isReady = true;
            }
        }

        public class TimelinePoint
        {
            float _inTime;
            float _outTime;
            
            private SeparateAnimation _sa;

            public TimelinePoint(SeparateAnimation sa)
            {
                if (sa != null)
                {
                    _sa = sa;
                    _inTime = sa.GetDelay();
                    _outTime = _inTime + sa.GetDuration();
                }
            }           

            
            public SeparateAnimation GetSA()
            {
                return _sa;
            }            
            public float GetInTime()
            {
                return _inTime;
            }
            public float GetOutTime()
            {
                return _outTime;
            }

            public void UpdateBounds()
            {               
                _inTime = _sa.GetDelay() + Time.time;
                _outTime = _inTime + _sa.GetDuration();
                //Debug.Log("UpdateBounds: _inTime:" + _inTime + " _outTime:" + _outTime + "   Time:" + Time.time);
            }
        }
    }
    
    private class PositionAnimation : SeparateAnimation
    {
        //private Vector3 _startP;
        private Vector3 _endP;

        public PositionAnimation(GameObject target, Vector3 endP, float duration = 1.0f, float delay = 0, EasingType eType = EasingType.LinearEasing)
        {
            _target = target.transform;
            //_startP = target.transform.position;
            _endP = endP;
            _duration = duration;
            _speed = 1 / duration;
            _delay = delay;

            _curve = new AnimationCurve();
            SetEasing(_curve, eType);
        }

       
        public override void UpdateAn(float frac)
        {
            //Debug.Log("PositionAnimation Time " + Time.time + " : frac = " + frac + "  curveY = " + _curve.Evaluate(frac));
            _target.position = Vector3.Lerp(startPosition, _endP, _curve.Evaluate(frac));
            //_target.transform.position = Vector3.Lerp(startPosition, _endP, frac);
        }

    }

    private class LocalPositionAnimation : SeparateAnimation
    {
        //private Vector3 _startLP;
        private Vector3 _endLP;

        public LocalPositionAnimation(GameObject target, Vector3 endLP, float duration = 1.0f, float delay = 0, EasingType eType = EasingType.LinearEasing)
        {
            _target = target.transform;

            //_startLP = target.transform.localPosition;
            _endLP = endLP;
            _duration = duration;
            _speed = 1 / duration;
            _delay = delay;

            _curve = new AnimationCurve();
            SetEasing(_curve, eType);
        }

        public override void UpdateAn(float frac)
        {
            //Debug.Log("LocalPositionAnimation Time " + Time.time + " : frac = " + frac);
            _target.localPosition = Vector3.Lerp(startLocalPosition, _endLP, _curve.Evaluate(frac));
        }
    }

    private class ShakeLocalPositionAnimation : SeparateAnimation
    {
        //private Vector3 _startLP;
        //private Vector3 _endLP;
        private Vector3 _strength;

        public ShakeLocalPositionAnimation(GameObject target, Vector3 strength, float duration = 1.0f, float delay = 0, EasingType eType = EasingType.LinearEasing)
        {
            _target = target.transform;

            //_startLP = target.transform.localPosition;
            //_endLP = endLP;
            _strength = strength;
            _duration = duration;
            _speed = 1 / duration;
            _delay = delay;


            _curve = new AnimationCurve();
            SetEasing(_curve, eType);
        }

        public override void UpdateAn(float frac)
        {
            //Debug.Log("LocalPositionAnimation Time " + Time.time + " : frac = " + frac);
            //_target.transform.localPosition = Vector3.Lerp(startLocalPosition, _endLP, _curve.Evaluate(frac));

            float _vibrate = 24f;
            
            float offsetX = _strength.x * Mathf.Sin(frac * 180f / (float)Math.PI * _vibrate) * _curve.Evaluate(frac);
            float offsetY = _strength.y * Mathf.Sin(frac * 180f / (float)Math.PI * _vibrate * 0.9f) * _curve.Evaluate(frac);
            float offsetZ = _strength.z * Mathf.Sin(frac * 180f / (float)Math.PI * _vibrate * 0.8f) * _curve.Evaluate(frac);

            _target.localPosition = startLocalPosition + new Vector3(offsetX, offsetY, offsetZ);
        }
    }

    private class RotateAnimation : SeparateAnimation
    {
        private Quaternion _deltaQ;
        
        public RotateAnimation(GameObject target, Quaternion deltaQ, float duration = 1.0f, float delay = 0, EasingType eType = EasingType.LinearEasing)
        {
            _target = target.transform;
            //_startQ = target.transform.rotation;
            //_deltaQ = Quaternion.FromToRotation(_target.transform.forward, endDirection);
            _deltaQ = deltaQ;
            _duration = duration;
            _speed = 1 / duration;
            _delay = delay;


            _curve = new AnimationCurve();
            SetEasing(_curve, eType);
        }

        public override void UpdateAn(float frac)
        {
            //Debug.Log("RotateAnimation  Time " + Time.time + " : frac = " + frac);
             _target.rotation = Quaternion.Lerp(startRotation, _deltaQ * startRotation, _curve.Evaluate(frac));
            //_target.transform.rotation = Quaternion.Lerp(startRotation, startRotation * _deltaQ, frac);
        }
    }

    private class LocalRotateAnimation : SeparateAnimation
    {
        private Quaternion _deltaQ;
        
        public LocalRotateAnimation(GameObject target, Quaternion deltaQ, float duration = 1.0f, float delay = 0, EasingType eType = EasingType.LinearEasing)
        {
            _target = target.transform;
            //_startQ = target.transform.rotation;
            //_deltaQ = Quaternion.FromToRotation(_target.transform.forward, endDirection);
            _deltaQ = deltaQ;
            _duration = duration;
            _speed = 1 / duration;
            _delay = delay;


            _curve = new AnimationCurve();
            SetEasing(_curve, eType);
        }

        
        public override void UpdateAn(float frac)
        {
            //Debug.Log("LocalRotateAnimation  Time " + Time.time + " : frac = " + frac + "   Euler.x " + (Quaternion.ToEulerAngles(_target.transform.localRotation).x / Mathf.PI * 180) + " y " + +(Quaternion.ToEulerAngles(_target.transform.localRotation).y / Mathf.PI * 180) + " z " + +(Quaternion.ToEulerAngles(_target.transform.localRotation).z / Mathf.PI * 180));

            _target.localRotation = Quaternion.Lerp(startLocalRotation, _deltaQ * startLocalRotation, _curve.Evaluate(frac));
            //Debug.Log("LocalRotateAnimation  Time " + Time.time + " : frac = " + frac + "   Euler.x " + (Quaternion.ToEulerAngles(_target.transform.localRotation).x /Mathf.PI *180) + " y " + +(Quaternion.ToEulerAngles(_target.transform.localRotation).y / Mathf.PI * 180) + " z " + +(Quaternion.ToEulerAngles(_target.transform.localRotation).z / Mathf.PI * 180));
            //_target.transform.localRotation = Quaternion.Lerp(startLocalRotation, startLocalRotation * _deltaQ, frac);
        }
    }

    private class ShakeLocalRotateAnimation : SeparateAnimation
    {
        private Quaternion _deltaQ;
        private Vector3 _strength;

        public ShakeLocalRotateAnimation(GameObject target, Vector3 strength, float duration = 1.0f, float delay = 0, EasingType eType = EasingType.CubicOutInverseEasing)
        {
            _target = target.transform;
            //_startQ = target.transform.rotation;
            //_deltaQ = Quaternion.FromToRotation(_target.transform.forward, endDirection);
            _duration = duration;
            _speed = 1 / duration;
            _delay = delay;
            _strength = strength;


            _curve = new AnimationCurve();
            SetEasing(_curve, eType);
        }


        public override void UpdateAn(float frac)
        {
            float _vibrate = 12f;
            
            //_deltaQ = Quaternion.LookRotation((_strength);
            float deltaAngleX = _strength.x * Mathf.Sin(frac * 180f / (float)Math.PI * _vibrate) * _curve.Evaluate(frac);
            float deltaAngleY = _strength.y * Mathf.Sin(frac * 180f / (float)Math.PI * _vibrate * 0.9f) * _curve.Evaluate(frac);
            float deltaAngleZ = _strength.z * Mathf.Sin(frac * 180f / (float)Math.PI * _vibrate * 0.8f) * _curve.Evaluate(frac);

            //Debug.Log("deltaAngleX:" + deltaAngleX + "  deltaAngleY:" + deltaAngleY + "  deltaAngleZ:" + deltaAngleZ + "  curve:" + _curve.Evaluate(frac));
            _target.localRotation = startLocalRotation * Quaternion.Euler(deltaAngleX, deltaAngleY, deltaAngleZ);
            
        }
    }

    public abstract class SeparateAnimation
    {
        protected Transform _target;
        protected AnimationCurve _curve;
        
        protected float _duration;
        protected float _speed;
        protected float _delay;

        protected EasingType _easingType;

        protected Vector3 startPosition;
        protected Vector3 startLocalPosition;
        protected Quaternion startRotation;
        protected Quaternion startLocalRotation;

        protected float elapsedTime;
        protected float startTime;

        protected bool _finished = false;
        protected bool _started = false;


        public abstract void UpdateAn(float frac);
        public void SetFinished(bool finished)
        {
            if (_finished != finished)
            {
                _finished = finished;
            }
        }
        public bool GetFinished()
        {
            return _finished;
        }
        public void SetStarted(bool started)
        {
            if (_started != started)
            {
                _started = started;

                if (_started)
                {
                    startPosition = _target.position;
                    startLocalPosition = _target.localPosition;
                    startRotation = _target.rotation;
                    startLocalRotation = _target.localRotation;
                }
            }
        }
        public bool GetStarted()
        {
            return _started;
        }
        public float GetDelay()
        {
            return _delay;
        }
        public float GetDuration()
        {
            return _duration;
        }

        protected void SetEasing(AnimationCurve curve, EasingType eType)
        {
            switch (eType)
            {
                case EasingType.LinearEasing:
                    SetLinearEasing(curve);
                    break;

                case EasingType.QuadInEasing:
                    SetQuadInEasing(curve);
                    break;

                case EasingType.QuadOutEasing:
                    SetQuadOutEasing(curve);
                    break;

                case EasingType.CubicInEasing:
                    SetCubicInEasing(curve);
                    break;

                case EasingType.CubicOutEasing:
                    SetCubicOutEasing(curve);
                    break;

                case EasingType.CubicOutInverseEasing:
                    SetCubicOutInverseEasing(curve);
                    break;
            }
        }
        protected void SetCubicOutEasing(AnimationCurve curve)
        {
            float step = (float)1 / (float)EASING_CURVE_STEPS;
            float x, y;

            //Debug.Log("step:" + step);

            for (int i = 0; i <= EASING_CURVE_STEPS; i++)
            {
                x = (float)i * step;
                y = 1 - x * x * x;

                Keyframe key = new Keyframe(1 - x, y);
                key.weightedMode = WeightedMode.Both;
                curve.AddKey(key);

                //Debug.Log("i: " + i + "  x:" + x + "  y:" + y);
                // easingCurve[easingCurveSteps - i] = y;
            }

            curve.preWrapMode = WrapMode.Once;
            curve.postWrapMode = WrapMode.Once;
            //Debug.Log("curve test 0.5  " + curve.Evaluate(0.5f));
        }

        protected void SetCubicOutInverseEasing(AnimationCurve curve)
        {
            float step = (float)1 / (float)EASING_CURVE_STEPS;
            float x, y;

            //Debug.Log("step:" + step);

            for (int i = 0; i <= EASING_CURVE_STEPS; i++)
            {
                x = (float)i * step;
                y = x * x * x;

                Keyframe key = new Keyframe(1 - x, y);
                key.weightedMode = WeightedMode.Both;
                curve.AddKey(key);

                //Debug.Log("i: " + i + "  x:" + x + "  y:" + y);
                // easingCurve[easingCurveSteps - i] = y;
            }

            curve.preWrapMode = WrapMode.Once;
            curve.postWrapMode = WrapMode.Once;
            //Debug.Log("curve test 0.5  " + curve.Evaluate(0.5f));
        }
        protected void SetQuadInEasing(AnimationCurve curve)
        {
            float step = (float)1 / (float)EASING_CURVE_STEPS;
            float x, y;

            for (int i = 0; i <= EASING_CURVE_STEPS; i++)
            {
                x = (float)i * step;
                y = 1 - x * x;

                Keyframe key = new Keyframe(x, y);
                key.weightedMode = WeightedMode.Both;
                curve.AddKey(key);
            }
        }

        protected void SetQuadOutEasing(AnimationCurve curve)
        {
            float step = (float)1 / (float)EASING_CURVE_STEPS;
            float x, y;

            for (int i = 0; i <= EASING_CURVE_STEPS; i++)
            {
                x = (float)i * step;
                y = 1 - x * x;

                Keyframe key = new Keyframe(1 - x, y);
                key.weightedMode = WeightedMode.Both;
                curve.AddKey(key);
            }
            curve.preWrapMode = WrapMode.Once;
            curve.postWrapMode = WrapMode.Once;
        }

        protected void SetCubicInEasing(AnimationCurve curve)
        {
            float step = (float)1 / (float)EASING_CURVE_STEPS;
            float x, y;

            for (int i = 0; i <= EASING_CURVE_STEPS; i++)
            {
                x = (float)i * step;
                y = 1 - x * x * x;

                Keyframe key = new Keyframe(x, y);
                key.weightedMode = WeightedMode.Both;
                curve.AddKey(key);
            }
            curve.preWrapMode = WrapMode.Once;
            curve.postWrapMode = WrapMode.Once;
        }

        protected void SetLinearEasing(AnimationCurve curve)
        {
            float step = (float)1 / (float)EASING_CURVE_STEPS;
            float x, y;

            for (int i = 0; i <= EASING_CURVE_STEPS; i++)
            {
                x = (float)i * step;
                y = x;

                Keyframe key = new Keyframe(x, y);
                key.weightedMode = WeightedMode.Both;
                curve.AddKey(key);
            }
            curve.preWrapMode = WrapMode.Once;
            curve.postWrapMode = WrapMode.Once;

        }

        

    }

    public static TweenController Instance
    {
        get
        {
            if (_instance == null)
                Debug.LogError("LerpAnimationController is null!");

            return _instance;
        }
    }   

    private TimelineController CreateTimelineController()
    {
        if(timelineController == null) timelineController = new TimelineController();
        
        return timelineController;
    }

    private void Awake()
    {
        _instance = this;
        CreateTimelineController();
    }

    private void Update()
    {
        if (timelineController != null) timelineController.CheckAnimation(); 
    }

}
